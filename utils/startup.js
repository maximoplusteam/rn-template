/**
 * This script will run after boot.js. Put all the initializations here
 * One example will be the database preload:
 * const preparedScript = script.replace(/\n/g, "");
 * prepareSQLDB(db => {
 *  console.log("PREPARING THE DB");
 *  return scriptRunner(db, preparedScript); //it should return promise
 * });
 *
 */
console.log("running startup");

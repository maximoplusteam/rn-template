import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Input, Button } from "react-native-elements";
import NavigationService from "../navigation/NavigationService";
import { showMessage, hideMessage } from "react-native-flash-message";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { username: "", password: "", error: "" };
  }

  login = () => {
    maximoplus.core.max_login(
      this.state.username,
      this.state.password,
      () => {
        NavigationService.navigate("Main");
      }, //TODO navigate to App stack in navigator
      //      err => this.setState({ error: "Invalid Username or Password" })
      err =>
        showMessage({
          message: "Invalid Username or Password",
          type: "error",
          position: "top",
          duration: 2000,
          color: "white",
          backgroundColor: "red",
          icon: "warning"
        })
    );
  };
  render() {
    return (
      <View>
        <View>
          <Text>{this.state.error}</Text>
        </View>
        <View style={{ marginLeft: "10%", marginTop: "10%" }}>
          <Image
            resizeMode="contain"
            style={{ width: "80%", height: "40%" }}
            source={require("../assets/images/color_logo_transparent_background.png")}
          />
        </View>
        <View style={{ marginTop: "20%", marginLeft: "5%", marginRight: "5%" }}>
          <Input
            placeholder="Username"
            leftIcon={{ type: "font-awesome", name: "user" }}
            leftIconContainerStyle={{ marginRight: 5 }}
            onChangeText={text => this.setState({ username: text })}
          />
          <Input
            placeholder="Password"
            secureTextEntry={true}
            leftIcon={{ type: "font-awesome", name: "lock" }}
            leftIconContainerStyle={{ marginRight: 5 }}
            onChangeText={text => this.setState({ password: text })}
          />
          <View style={{ marginTop: "20%" }}>
            <Button title="Sign In" onPress={this.login} />
          </View>
        </View>
      </View>
    );
  }
}

import React from "react";
import { createStackNavigator } from "react-navigation-stack";
import ListDialog from "../dialogs/ListDialog";
import QbeListDialog from "../dialogs/QbeListDialog";
import WorkflowDialog from "../dialogs/WorkflowDialog";
//import PhotoUpload from "../dialogs/PhotoUpload";
//import DocumentUpload from "../dialogs/DocumentUploader";
//import DocumentViewer from "../dialogs/DocumentViewer";
//import BarcodeScan from "../dialogs/BarcodeScan";
import OfflineError from "../dialogs/OfflineErrorDialog";

const DialogStack = createStackNavigator({
  list: { screen: ListDialog, title: "Select a value" },
  qbelist: { screen: QbeListDialog, title: "Select one or more values" },
  workflow: { screen: WorkflowDialog }
  //photoUpload: { screen: PhotoUpload },
  //documentUpload: { screen: DocumentUpload },
  //  documentViewer: { screen: DocumentViewer },
  //barcodeScan: { screen: BarcodeScan },
  //offlineError: { screen: OfflineError }
});

export default DialogStack;

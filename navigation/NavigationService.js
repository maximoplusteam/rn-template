import { NavigationActions, StackActions } from "react-navigation";

//StackActins will be used for the dialogs in this template
let navigatorResolve = null;
let navigatorReject = null;
const _navigator = new Promise((resolve, reject) => {
  navigatorResolve = resolve;
  navigatorReject = reject;
});

function setTopLevelNavigator(navigatorRef) {
  navigatorResolve(navigatorRef);
}

function navigate(routeName, params) {
  _navigator.then(navigator =>
    navigator.dispatch(
      NavigationActions.navigate({
        routeName,
        params
      })
    )
  );
}

function push(routeName, params) {
  _navigator.then(navigator =>
    navigator.dispatch(StackActions.push({ routeName, params }))
  );
}

function goBack() {
  _navigator.then(navigator => navigator.dispatch(NavigationActions.back({})));
}

function openDialog(dialogName, params) {
  _navigator.then(navigator =>
    navigator.dispatch(
      NavigationActions.navigate({
        routeName: "Dialogs",
        params: {},
        action: StackActions.push({ routeName: dialogName, params })
      })
    )
  );
}

function closeDialog(last) {
  //if the last dialog from the dialg stack has been closed, go back
  const actionAfter = last ? { action: NavigationActions.back() } : {};
  _navigator.then(navigator =>
    navigator.dispatch(StackActions.pop(actionAfter))
  );
}

function pop() {
  _navigator.then(navigator => navigator.dispatch(StackActions.pop({ n: 1 })));
}

// add other navigation functions that you need and export them

export default {
  navigate,
  setTopLevelNavigator,
  push,
  pop,
  goBack,
  openDialog,
  closeDialog
};

import React from "react";
import { Platform } from "react-native";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";

import TabBarIcon from "../components/TabBarIcon";
import ListScreen from "../screens/ListScreen";
import SectionScreen from "../screens/SectionScreen";
import QbeSectionScreen from "../screens/QbeSectionScreen";

const config = Platform.select({
  web: { headerMode: "screen" },
  default: {}
});

const ListStack = createStackNavigator({ List: ListScreen }, config);
ListStack.navigationOptions = {
  tabBarLabel: "List",
  tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-list" />
};
ListStack.path = "";

const SectionStack = createStackNavigator({ Section: SectionScreen }, config);
SectionStack.navigationOptions = {
  tabBarLabel: "Details",
  tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-link" />
};

const QBESectionStack = createStackNavigator(
  { QBESection: QbeSectionScreen },
  config
);
QBESectionStack.navigationOptions = {
  tabBarLabel: "Search",
  tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-search" />
};

const tabNavigator = createBottomTabNavigator(
  {
    ListStack,
    SectionStack,
    QBESectionStack
  },
  {
    transitionConfig: () => ({
      transitionSpec: {
        duration: 0
      }
    })
  }
);

tabNavigator.path = "";

export default tabNavigator;

import React from "react";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import MainTabNavigator from "./MainTabNavigator";
import LoginScreen from "../screens/Login";
import DialogNavigator from "./DialogNavigation";

const AuthStack = createStackNavigator(
  { Login: LoginScreen },
  { headerMode: "none" }
);

//Root stack navigator will have tabs and dialogs, plus the other modal required (for example for the barcode scanner)
const RootStackNavigator = createStackNavigator(
  {
    Main: MainTabNavigator,
    Dialogs: DialogNavigator
  },
  { initialRootName: "Main", headerMode: "none" }
);

export default createAppContainer(
  createSwitchNavigator(
    {
      // You could add another route here for authentication.
      // Read more at https://reactnavigation.org/docs/en/auth-flow.html
      Root: RootStackNavigator,
      Auth: AuthStack
    },
    { initialRouteName: "Root" }
  )
);

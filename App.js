import { AppLoading } from "expo";
import { Asset } from "expo-asset";
import * as Font from "expo-font";
import React, { useState } from "react";
import { Platform, StatusBar, StyleSheet, View } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { setDialogHolder } from "./utils/utils";
import Containers from "./Containers";
import AppNavigator from "./navigation/AppNavigator";
import NavigationService from "./navigation/NavigationService";
import Spinner from "react-native-loading-spinner-overlay";

import "./utils/boot";

import {
  AppContainer,
  SingleMboContainer,
  RelContainer,
  setExternalRootContext
} from "mplus-react";
import { ContextPool } from "react-multiple-contexts";
//import { useScreens } from "react-native-screens";
//useScreens();

import DialogHolder from "./screens/Dialogs";
import FlashMessage from "react-native-flash-message";
import { SERVER_ROOT } from "react-native-dotenv";
import "./utils/db";
import "./utils/offline";
import "./utils/startup";

const rootContext = React.createContext(null);
setExternalRootContext(rootContext);

maximoplus.net.globalFunctions.serverRoot = () => {
  return SERVER_ROOT;
};

maximoplus.core.setOnLoggedOff(err => NavigationService.navigate("Login"));

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  const [waitCursor, setWaitCursor] = useState(false);

  maximoplus.core.globalFunctions.globalDisplayWaitCursor = () => {
    setWaitCursor(true);
  };

  maximoplus.core.globalFunctions.globalRemoveWaitCursor = () => {
    setWaitCursor(false);
  };

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return (
      <ContextPool rootContext={rootContext} initialSize={10} minimumFree={3}>
        <Containers />
        <DialogHolder
          ref={dialogHolderRef => {
            setDialogHolder(dialogHolderRef);
          }}
        />
        <View style={styles.container}>
          <Spinner visible={waitCursor} />
          {Platform.OS === "ios" && <StatusBar barStyle="default" />}
          <AppNavigator
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
          />
          <FlashMessage position="bottom" />
        </View>
      </ContextPool>
    );
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require("./assets/images/robot-dev.png"),
      require("./assets/images/robot-prod.png")
    ]),
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      ...Ionicons.font,
      // We include SpaceMono because we use it in HomeScreen.js. Feel free to
      // remove this if you are not using it in your app
      "space-mono": require("./assets/fonts/SpaceMono-Regular.ttf")
    })
  ]);
}

function handleLoadingError(error: Error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  }
});
